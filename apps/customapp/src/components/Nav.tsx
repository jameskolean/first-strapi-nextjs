import Link from "next/link";
export default async function Nav(props: unknown) {
  return (
    <nav className="max-w-screen-md mx-auto">
      <ul className="flex flex-row gap-4 justify-center">
        <li>
          <Link href="/">Home</Link>
        </li>
        <li>
          <Link href="/dashboard">Dashboard</Link>
        </li>
      </ul>
    </nav>
  );
}
