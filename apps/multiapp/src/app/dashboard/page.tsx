import { fetcher } from "../../components/util";
import BasicDashboard from "../../components/Dashboards/BasicDashboard";
import AcmeDashboard from "../../components/Dashboards/AcemDashboard";
import RoadRunnerDashboard from "../../components/Dashboards/RoadRunnerDashboard";
async function getData() {
  return fetcher(
    `/dashboards?filters[tenants][name][$contains]=${process.env.TENANT}`
  );
}
// interface Props {
//   params: {
//     slug: string;
//   };
// }
export default async function Page() {
  const { data } = await getData();
  console.log(data);
  let dashboardComponent = "BasicDashboard";
  if (data.length > 0) {
    dashboardComponent = data[0].attributes.component;
  }

  return (
    <div>
      <h1>Dashboard</h1>
      {dashboardComponent === "BasicDashboard" && <BasicDashboard />}
      {dashboardComponent === "AcmeDashboard" && <AcmeDashboard />}
      {dashboardComponent === "RoadRunnerDashboard" && <RoadRunnerDashboard />}
    </div>
  );
}
