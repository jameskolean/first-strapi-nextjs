import { fetcher } from "../../../components/util";
import Link from "next/link";

export async function generateStaticParams() {
  const { data } = await fetcher(
    `/pages?populate=menu&filters[menu][tenants][name][$contains]=${process.env.TENANT}`
  );
  return data.map((page: any) => ({
    slug: page.attributes.menu.data.attributes.slug,
  }));
}

async function getData(slug: string) {
  return fetcher(`/pages?filters[menu][slug][$contains]=${slug}`);
}
interface Props {
  params: {
    slug: string;
  };
}
export default async function Page(props: Props) {
  const { data } = await getData(props.params.slug);
  const page = data[0];

  return (
    <div className="flex flex-col items-center justify-center py-20 gap-10">
      <h1>{page.attributes.name}</h1>
      <div>{page.attributes.content}</div>
    </div>
  );
}
