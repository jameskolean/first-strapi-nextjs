import { fetcher } from "../components/util";
import Link from "next/link";
async function getData() {
  return fetcher(
    `/menus?filters[tenants][name][$contains]=${process.env.TENANT}`
  );
}
export default async function Nav(props: unknown) {
  const { data } = await getData();
  return (
    <nav className="max-w-screen-md mx-auto">
      <ul className="flex flex-row gap-4 justify-center">
        <li>
          <Link href="/">Home</Link>
        </li>
        {data.map((menu: any) => (
          <li key={menu.id}>
            <Link
              href={`${menu.attributes.path ? menu.attributes.path : ""}${
                menu.attributes.slug
              }`}
            >
              {menu.attributes.name}
            </Link>
          </li>
        ))}
      </ul>
    </nav>
  );
}
