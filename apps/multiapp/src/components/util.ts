export async function fetcher(path: string) {
  const res = await fetch(`${process.env.STRAPI_API_URL}${path}`, {
    headers: {
      Authorization: `Bearer ${process.env.STRAPI_API_TOKEN}`,
    },
    next: { revalidate: 0 },
  });
  const json = await res.json();
  return json;
}
